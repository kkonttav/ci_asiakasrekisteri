drop database if exists asiakasrekisteri;

create database asiakasrekisteri;

use asiakasrekisteri;

create table kayttaja (
    id int primary key auto_increment,
    email varchar(100) not null unique,
    salasana varchar(255) not null,
    tallennettu timestamp default current_timestamp
) engine=innodb;

create table asiakas (
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    email varchar(100) not null,
    tallennettu timestamp default current_timestamp
) engine=innodb;


create table muistiinpano (
    id int primary key auto_increment,
    teksti text,
    paivays timestamp default current_timestamp
    on update current_timestamp,
    asiakas_id int not null,
    index (asiakas_id),
    foreign key (asiakas_id) references asiakas(id) on delete restrict
) engine=innodb;


insert into asiakas (etunimi,sukunimi,email)
values ('Kari', 'Konttavaara', 'kako@gmail.com');