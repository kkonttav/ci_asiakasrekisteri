<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muistiinpano_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function hae_kaikki($id) {
        $this->db->where('asiakas_id',$id);
        $kysely = $this->db->get('muistiinpano');
        return $kysely->result();
    }
    
    public function hae($id) {
        $this->db->where('id',$id);
        $kysely = $this->db->get('muistiinpano');
        return $kysely->row();
    }

    public function lisaa($data) {
        $this->db->insert('muistiinpano',$data);
        return $this->db->insert_id();
    }
    
    public function muokkaa($data) {
        $this->db->where('id',$data['id']);
        return $this->db->update('muistiinpano', $data);
    }
    
    public function poista($id) {
        $this->db->where('id',$id);
        $this->db->delete('muistiinpano');
    }
    
}
