<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kayttaja extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Kayttaja_model');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="label label-warning">','</div>');
    }
    
    public function index() {
        $this->kirjaudu();    
    }
    
    public function kirjaudu() {
        $data['otsikko'] = "Kirjaudu asiakasrekisteriin";
        $data['email'] = '';
        $data['salasana'] = '';
        $data['sisalto'] = 'kayttaja/kirjaudu_view';
        $this->load->view('template',$data);        
    }

    public function kirjaudu_ulos() {
        $this->session->unset_userdata('kayttaja');
        redirect('kayttaja/kirjaudu','refresh');
    }    
    
    public function rekisteroidy() {
        $data['otsikko'] = "Luo uusi käyttäjä asiakasrekisterille";
        $data['email'] = '';
        $data['salasana'] = '';
        $data['sisalto'] = 'kayttaja/rekisteroidy_view';
        $this->load->view('template',$data);         
    }

    public function tallenna() {
        $data=array(
            'email'     => $this->input->post('email'),
            'salasana'  => $this->input->post('salasana')
            );
        
        $this->form_validation->set_rules('email', 'email', 'required|max_length[100]|valid_email|is_unique[kayttaja.email]' );
        $this->form_validation->set_rules('salasana', 'salasana', 'required|min_length[8]');
        
        if($this->form_validation->run() == TRUE) {
            $this->Kayttaja_model->lisaa($data);
            redirect('kayttaja/kirjaudu','refresh');
            }
        else {
            $data['otsikko']=  "Käyttäjää ei voida tallentaa!";
            $data['sisalto'] = 'kayttaja/rekisteroidy_view';
            $this->load->view('template',$data);            
        }
    }
    
    public function tarkasta() {
        $email = $this->input->post('email');
        $salasana = $this->input->post('salasana');

        $kayttaja = $this->Kayttaja_model->hea_sahkopostilla_ja_salasanalla($email, $salasana);
        if($kayttaja) {
            $this->session->set_userdata('kayttaja',$kayttaja);
//            $this->session->set_userdata('kirjautunut', 'true');
            
            redirect('asiakas/index','refresh');
        }
        else {
            $data['otsikko']=  "Kirjautuminen epäonnistui!";
            $data['email'] = $email;
            $data['sisalto'] = 'kayttaja/kirjaudu_view';
            $this->load->view('template',$data);            
        }
    }

}