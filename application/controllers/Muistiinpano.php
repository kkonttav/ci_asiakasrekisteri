<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muistiinpano extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Muistiinpano_model');
        $this->load->model('Asiakas_model');
        $this->load->library('Util');
        
        if($this->session->flashdata('asiakas')) {
            $asiakas = $this->session->flashdata('asiakas');
            $this->session->set_flashdata('asiakas', $asiakas);    
        }
    }
    
    public function index($id="") {
        if($this->session->flashdata('asiakas')) {
            $asiakas = $this->session->flashdata('asiakas');
        }
        else {
            $asiakas = $this->Asiakas_model->hae($id);
            $this->session->set_flashdata('asiakas', $asiakas);
        }
        
        $muistiinpanot = $this->Muistiinpano_model->hae_kaikki($asiakas->id);
        if($asiakas) {
            $data['id'] = $asiakas->id;
            $data['etunimi'] = $asiakas->etunimi;
            $data['sukunimi'] = $asiakas->sukunimi;
            }
        $data['muistiinpanot']= $muistiinpanot;
        $data['otsikko'] = "Anna muistiinpano";
        $data['sisalto'] = 'muistiinpano/muistiinpano_view';
        $this->load->view('template',$data);        
    }

    public function tallenna() {
        $data=array(
            'teksti'        => $this->input->post('muistiinpano'),
            'id'            => $this->input->post('id'),
            );
        
            if(strlen($this->input->post('id')) > 0) {
                $this->Muistiinpano_model->muokkaa($data);
            }
            else {
                $this->Muistiinpano_model->lisaa($data);
            }        
        redirect("muistiinpano/index",'refresh');
    }

    public function varmista_poisto($id) {
        $muistiinpano = $this->Muistiinpano_model->hae($id);
        $data['id'] = $muistiinpano->id;
        $data['teksti'] = $muistiinpano->teksti;
        

        $data['otsikko']=  "Poista muistiinpano:";
        $data['kysymys'] ='Poistetaanko muistiinpano: ' . $muistiinpano->teksti . "?";
        $data['ok_osoite'] = site_url() . "muistiinpano/poista/$id";
        $data['peruuta_osoite'] =  site_url();
        
        $data['sisalto'] = 'varmistus_view';
        $this->load->view('template',$data);        
    }        

    public function poista($id) {
        $this->Muistiinpano_model->poista(intval($id));
        redirect("muistiinpano/index",'refresh');        
    }

    public function muokkaa($id) {
        $muistiinpano = $this->Muistiinpano_model->hae($id);
        if($muistiinpano) {
            $data['id'] = $muistiinpano->id;
            $data['teksti'] = $muistiinpano->teksti;
            $asiakas = $this->session->flashdata('asiakas');
            $data['sukunimi'] = $asiakas->sukunimi;
            $data['etunimi'] = $asiakas->etunimi;
            $data['otsikko']=  "Muokkaa asiakkaan muistiinpanoa";
            $data['sisalto'] = 'muistiinpano/muokkaa_view';
            $this->load->view('template',$data);
        }
        else {
            print "TODO oma view: Muistiinpanoa ei löydy";
        }

    }    
    
}