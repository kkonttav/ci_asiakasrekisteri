<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asiakas extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
//        if($this->session->has_userdata('kayttaja') == FALSE) { 
//            redirect('kayttaja/kirjaudu');
//            }
            
        $this->load->model('Asiakas_model');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->form_validation->set_error_delimiters('<div class="label label-warning">','</div>');
    }
    
    public function index() {
        $etsi='';
        $jarjestys="";
        if($this->session->userdata('jarjestys')) {
            $jarjestys=  $this->session->userdata('jarjestys');
            }
        
        if($this->uri->segment(4)) {
            $jarjestys=  $this->uri->segment(4);
            $this->session->set_userdata("jarjestys",$jarjestys);
            }
        
        if($this->input->post('etsi')) {
            $etsi = $this->input->post('etsi');
            $data['asiakkaat'] = $this->Asiakas_model->hae_kaikki($etsi);
            }
        else {
            $asiakkaita_sivulla=3;
            
            $data['asiakkaat'] = $this->Asiakas_model->hae_kaikki($etsi,$asiakkaita_sivulla,  $this->uri->segment(3),$jarjestys);
            $data['sivutuksen_kohta']=0;
            if($this->uri->segment(3)) {
                $data["sivutuksen_kohta"]=  $this->uri->segment(3);
                }
            $config['base_url']=  site_url('asiakas/index');
            $config['total_rows']=  $this->Asiakas_model->laske_asiakkaat();
            $config['per_page']=$asiakkaita_sivulla;
            $config['uri_segment']=3;
            $this->pagination->initialize($config);
            }

        $data['etsi']=$etsi;
        $data['otsikko'] = "Asiakasrekisteri";
        $data['sisalto'] = 'asiakas/asiakkaat_view';
        $this->load->view('template',$data);
    }
    public function lisaa() {
        $data['otsikko']=  "Lisää uusi asiakas";
        $data['id'] = '';
        $data['etunimi'] = '';
        $data['sukunimi'] = '';
        $data['email'] = '';
        $data['sisalto'] = 'asiakas/asiakas_view';
        $this->load->view('template',$data);
    }

    public function poista($id) {
        $this->Asiakas_model->poista(intval($id));
        redirect('asiakas/index','refresh');        
    }
    
    public function varmista_poisto($id) {
        $asiakas = $this->Asiakas_model->hae($id);
        $data['id'] = $asiakas->id;
        $data['etunimi'] = $asiakas->etunimi;
        $data['sukunimi'] = $asiakas->sukunimi;

        $data['otsikko']=  "Poista asiakas:";
        $data['kysymys'] ='Poistetaanko asiakas: ' . $asiakas->sukunimi . ", " . $asiakas->etunimi . "?";
        $data['ok_osoite'] = site_url() . "asiakas/poista/$id";
        $data['peruuta_osoite'] =  site_url();
        
        $data['sisalto'] = 'varmistus_view';
        $this->load->view('template',$data);        
    }

    public function muokkaa($id) {
        $asiakas = $this->Asiakas_model->hae($id);
        if($asiakas) {
            $data['id'] = $asiakas->id;
            $data['etunimi'] = $asiakas->etunimi;
            $data['sukunimi'] = $asiakas->sukunimi;
            $data['email'] = $asiakas->email;
            $data['otsikko']=  "Muokkaa asiakkaan tietoja";
            $data['sisalto'] = 'asiakas/asiakas_view';
            $this->load->view('template',$data);
        }
        else {
            print "TODO oma view: Asiakasta ei löydy";
        }

    }

    public function tallenna() {
        $data=array(
            'id'        => $this->input->post('id'),
            'etunimi'   => $this->input->post('etunimi'),
            'sukunimi'  => $this->input->post('sukunimi'),
            'email'     => $this->input->post('email')
        );
        
        $this->form_validation->set_rules('etunimi', 'etunimi', 'required|max_length[50]');
        $this->form_validation->set_rules('sukunimi', 'sukunimi', 'required|max_length[50]');
        $this->form_validation->set_rules('email', 'email', 'required|max_length[100]|valid_email|is_unique[asiakas.email]' );
        
        if($this->form_validation->run() == TRUE) {
            if(strlen($this->input->post('id')) > 0) {
                $this->Asiakas_model->Muokkaa($data);
            }
            else {
                $this->Asiakas_model->Lisaa($data);
            }
            redirect('asiakas/index','refresh');
        }
        else {
            $data['otsikko']=  "Tarkasta asiakas";
            $data['sisalto'] = 'asiakas/asiakas_view';
            $this->load->view('template',$data);            
        }
    }
    
        public function suorita() {
            $valinnat = $this->input->post('valinta');
            if(isset($valinnat)) {
                foreach ($valinnat as $valinta ) {
                    $this->Asiakas_model->poista($valinta);
                    }
                }                
            redirect('asiakas/index','refresh');
        }
}