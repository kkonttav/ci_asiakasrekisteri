<h3><?php echo $otsikko;?></h3>

<?php
// Näyttää kaikki formin errorit tässä
//echo validation_errors();
?>
<div class="row">
    <div class="col-lg-6 col-lg-offset-2">
        <form role="form" method="post" action="<?php echo site_url() . 'kayttaja/tarkasta' ?>">
            <div class="form-group">
                <label for="email">Sähköposti:</label>
                <input type="text" class="form-control" id="email" name="email" value="<?php echo $email;?>" maxlength="100" autofocus>
                <?php echo form_error('email');?>
            </div>
            <div class="form-group">
                <label for="salasana">Salasana:</label>
                <input type="password" class="form-control" id="salasana" name="salasana" maxlength="20">
                <?php echo form_error('salasana');?>
            </div>
            <button type="submit" class="btn btn-primary">Kirjaudu</button>
            <a href="<?php echo site_url() . 'kayttaja/rekisteroidy';?>">Rekisteröidy</a>
            <?php
            $osoite = site_url() . "kayttaja/rekisteroidy";
            print "<br />";
            print $osoite;
            print "<br />"; 
            print site_url();
            print "<br />";
            ?>
        </form>
    </div>
</div>    