<h3><?php echo $otsikko; ?></h3>
<form id="etsi_lomake"method="post" action="<?php echo site_url();?>asiakas/index">
    <input id="etsi" name="etsi" type="hidden">
</form>
    
<form method="post" action="<?php echo site_url();?>asiakas/suorita">
<div class="row">
    <div class="col-md-6">
        <div class="form-group has-feedback">
            <div class="form-group">
                <label for="search" class="sr-only">Search</label>
                <input name="search" id="search" type="text" class="form-control" >
                <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
        </div>
        <?php
        if(strlen($etsi)) {
        ?>
            <a href="<?php site_url(); ?>" class="btn btn-xs btn-default"><?php echo $etsi;?>;<span id="etsi_teksti" class="glyphicon glyphicon-remove"></span></a>
        <?php
        }
        ?>
    </div>
</div>    
    
    
<div class="row">    
    <div class="col-md-2">
        <a href="<?php echo site_url() . 'asiakas/lisaa';?> "class="btn btn-default">Lisää</a>
        <input type="submit" value="Poista" name="poista" class="pull-right btn btn-default">
    </div>
</div>    
<div class="row">
    <div class="col-md-2 col-lg-offset-2">
        <table class="table">
            <thead>
            <tr>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/sukunimi",'Sukunimi');?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/etunimi",'Etunimi');?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/email",'Sähköposti');?></th>
                <th>Poista</th>
                <th>Muokkaa</th>
                <th>Valinta</th>
                <th>Muistiinpano</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($asiakkaat as $asiakas) {
                echo "<tr>";
                    echo "<td>$asiakas->sukunimi</td>";
                    echo "<td>$asiakas->etunimi</td>";
                    echo "<td>$asiakas->email</td>";
?>
                    <td>
                    <a href='<?php echo site_url() . 'asiakas/varmista_poisto/' . $asiakas->id;?>'>
                    <span class='glyphicon glyphicon-trash'></span>
                    </td>
                    <td>
                    <a href='<?php echo site_url() . 'asiakas/muokkaa/' . $asiakas->id;?>'>
                    <span class="glyphicon glyphicon-edit"></span>
                    </td>
<?php
/* Opettajan malli miten tehdään php:ssa sama kuin yllä oleva html5!!

                      echo "<td><a href='" . site_url() .  "asiakas/varmista_poisto/" 
                         . "$asiakas->id'>"
                         . "<span class='glyphicon glyphicon-trash'></span>"
                         . "</a></td>";
*/
                echo "<td><input type='checkbox' name='valinta[]' value='$asiakas->id'></td>";
?>                
                <td>
                <a href='<?php echo site_url() . 'muistiinpano/index/' . $asiakas->id;?>'>
                <span class="glyphicon glyphicon-edit"></span>
                </td>
<?php                
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
        <?php
            echo $this->pagination->create_links();
        ?>
    </div>
</div>
</form>
    