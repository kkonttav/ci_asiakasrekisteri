<h3><?php echo $otsikko; ?></h3>

<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8">
        <p>
            <?php echo $kysymys; ?>
        </p>

        <a href="<?php echo $ok_osoite; ?>"
           class="btn btn-primary"> OK
        </a>
        <a href="<?php echo $peruuta_osoite; ?>"
           class="btn btn-default"> Peruuta
        </a>
    </div>
</div>