<h3><?php echo $otsikko; ?></h3>

<?php
// Näyttää kaikki formin errorit tässä
//echo validation_errors();
?>

<form role="form" method="post" action="<?php echo site_url() . 'muistiinpano/tallenna' ?>">
    <div class="form-group">
        <label for="asiakas">Asiakas:</label>
        <label for="asiakas"><?php echo $etunimi; ?></label>
        <label for="asiakas"><?php echo $sukunimi; ?></label>
    </div>
    
    <div class="form-group">
        <label for="teksti">Teksti:</label>
        <textarea type="text" class="form-control" name="muistiinpano" id="muistiinpano"><?php echo $teksti;?></textarea>
    </div>
    <div class="form-group">
        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $id?>" >
    </div>
    <button type="submit" class="btn btn-primary">Tallenna</button>
    <a href="<?php echo site_url() . 'muistiinpano';?> "class="btn btn-default">Peruuta</a>
</form>
