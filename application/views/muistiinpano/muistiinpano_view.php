<h3><?php echo $otsikko; ?></h3>

<?php
// Näyttää kaikki formin errorit tässä
//echo validation_errors();
?>

<form role="form" method="post" action="<?php echo site_url() . 'muistiinpano/tallenna' ?>">
    <div class="form-group">
        <label for="asiakas">Asiakas:</label>
        <label for="asiakas"><?php echo $etunimi; ?></label>
        <label for="asiakas"><?php echo $sukunimi; ?></label>
    </div>
    
    <div class="form-group">
        <label for="teksti">Teksti:</label>
        <textarea type="text" class="form-control" name="muistiinpano" id="muistiinpano" autofocus=""></textarea>
    </div>
    <div class="form-group">
        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $id?>" >
    </div>
    <button type="submit" class="btn btn-primary">Tallenna</button>
    <a href="<?php echo site_url() . 'asiakas';?> "class="btn btn-default">Peruuta</a>
</form>


<table class="table">
    <thead>
    <tr>
        <th>Aika</th>
        <th>Teksti</th>
        <th>Muokkaa</th>
        <th>Poista</th>
    </tr>
    </thead>
    <tbody>
        <?php
        foreach ($muistiinpanot as $muistiinpano) {
            echo "<tr>";
            $aika = $this->util->format_sqldate_to_fin($muistiinpano->paivays);
            echo "<td>$aika</td>";
            echo "<td>$muistiinpano->teksti</td>";
            ?>
            <td>
            <a href='<?php echo site_url() . 'muistiinpano/muokkaa/' . $muistiinpano->id;?>'>
            <span class='glyphicon glyphicon-edit'></span>
            </td>
        
            <td>
            <a href='<?php echo site_url() . 'muistiinpano/varmista_poisto/' . $muistiinpano->id;?>'>
            <span class='glyphicon glyphicon-trash'></span>
            </td>
        <?php
        echo "</tr>";
        }
        ?>
    </tbody>
</table>
  
    